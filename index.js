const got = require('got');
const url = require('url');
const pForever = require('p-forever');
const delay = require('delay');

const createApiUrl = (jira, sprintId) => url.resolve(jira.url, `/rest/agile/1.0/sprint/${sprintId}/issue`);

const createHeaders = jira => ({
    auth: `${jira.user}:${jira.pass}`,
    json: true
});

const getIssuesFromSprint = (jira, sprintId) => got(createApiUrl(jira, sprintId), createHeaders(jira)).then(response => response.body.issues);

const getIssueKeys = issues => issues.map(issue => issue.key);

const getAllIssueKeys = (jira, sprintId) => getIssuesFromSprint(jira, sprintId).then(getIssueKeys);

const getNewIssues = (jira, sprintId, existingKeys) => getIssuesFromSprint(jira, sprintId).then(issues => issues.filter(issue => !existingKeys.includes(issue.key)));

const pollIssues = (jira, sprintId, callback) => {
    getAllIssueKeys(jira, sprintId).then(existingKeys => {
        pForever(previousKeys => {
            return delay(5000)
                .then(() => getNewIssues(jira, sprintId, previousKeys))
                .then(newIssues => {
                    if (newIssues.length > 0) {
                        callback(newIssues);
                    }
                    return newIssues;
                })
                .then(getIssueKeys)
                .then(keys => {
                    return [].concat(previousKeys, keys);
                });
        }, existingKeys);
    });
}

module.exports = {
    pollIssues
};
